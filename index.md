---
title: Accueil
layout: accueil.liquid
pagination:
    include: All
    per_page: 10
    sort_by: ["published_date"]
    date_index: ["Year", "Month"]
    permalink_suffix: "./{{ num }}/"
---


{% for page in paginator.pages %}

## [{{page.title}}]({{page.permalink}})
{{page.description}}
{% endfor %}

<div class="pagination">
{% if paginator.previous_index %}
<a href="/{{ paginator.first_index_permalink }}">Articles récents</a>
{% endif %}
{% if paginator.next_index %}
<a href="/{{ paginator.last_index_permalink }}">Articles précedents</a>
{% endif %}
</div>
