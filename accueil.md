---
title: Accueil
layout: accueil.liquid
---
## Bienvenue !

{% for post in collections.posts.pages %}
#### [{{post.title}}]|({{ post.permalink }})

{{ post.description }}

{% endfor %}
